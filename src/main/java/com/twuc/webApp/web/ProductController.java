package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ProductController {
    @Autowired
    ProductRepository productRepository;
    @PostMapping("/api/products")
     public ResponseEntity creatProduct(@RequestBody @Valid CreateProductRequest request){
        Product product = productRepository.save(new Product(request.getName(), request.getPrice(),request.getUnit()));
        productRepository.flush();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("location", "http://localhost/api/products/"+product.getId())
                .build();
    }

    @GetMapping("/api/products/{productId}")
    public ResponseEntity<Product> getProductId(@PathVariable Long productId){
        Optional<Product> id = productRepository.findById(productId);
        if(!id.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(id.get());
    }
}