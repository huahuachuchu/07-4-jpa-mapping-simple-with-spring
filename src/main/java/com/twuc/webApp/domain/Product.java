package com.twuc.webApp.domain;

import javax.persistence.*;
@Entity
public class Product {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Integer price;
    @Column(nullable = false)
    private String unit;
    public Product(String name, Integer price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}